import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
//import { RoomModule } from './room/room.module';
import { OfficeModule } from './office/office.module';
import { MongooseModule } from '@nestjs/mongoose';
import { OfficeSchema } from './office/shemas/office.shema';
import { RoomModule } from './room/room.module';
//import { RoomSchema } from './room/shemas/room.shema';
import { DeskModule } from './desk/desk.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
//import { LocalAuthService } from './local.auth/local.auth.service';
import { BookingModule } from './booking/booking.module';
import { UserService } from './user/user.service';
import { PasswordModule } from './password/password.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';


@Module({
  imports: [RoomModule,
    OfficeModule,
    MongooseModule.forRoot('mongodb://127.0.0.1/app'),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname,'../templates'),
    }),
    DeskModule,
    UserModule,
    AuthModule,
    BookingModule,
    PasswordModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { 
  // constructor(){
  //   console.log(join(__dirname,'../templates'))
  // }
}
