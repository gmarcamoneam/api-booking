import { Module } from '@nestjs/common';
import { PasswordService } from './password.service';
import { PasswordController } from './password.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Password, PasswordSchema } from './shemas/password.shema';
import { MailerModule } from '@nestjs-modules/mailer';
// import { Password } from './entities/password.entity';

@Module({
  imports: [PasswordModule,MailerModule.forRoot({transport:
    {
      host:'localhost',
      port:1025
    },
    // defaults: {
    //   from: '"No Reply" <no-reply@localhost>',
    // },
  }),MongooseModule.forFeature([{ name: Password.name, schema: PasswordSchema }])],
  controllers: [PasswordController],
  providers: [PasswordService]
})
export class PasswordModule {}
