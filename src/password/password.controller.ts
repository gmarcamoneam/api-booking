import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PasswordService } from './password.service';
import { CreatePasswordDto } from './dto/create-password.dto';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { MailerService } from '@nestjs-modules/mailer/dist/mailer.service';

@Controller('password')
export class PasswordController {
  constructor(private readonly passwordService: PasswordService,private maileservice:MailerService) {}

  // @Post()
  // create(@Body() createPasswordDto: CreatePasswordDto) {
  //   return this.passwordService.create(createPasswordDto);
  // }
  // @Post('/forget')
  // async forget(@Body() createPasswordDto: CreatePasswordDto){
  //   const token = Math.random().toString(20).substring(2,12)
  //   await this.passwordService.create(createPasswordDto)
  //   const url = `http://localhost:4200/reset/${token}`
  //   await this.maileservice.sendMail({
  //     to: createPasswordDto.email,
  //     subject: 'Reset your password',
  //     html:" Click <a href=`${url}`>here</a> to reset your password! "
  //   })
  //   return {
  //     message:"plase check your email"
  //   }

  // }
  // @Get()
  // findAll() {
  //   return this.passwordService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.passwordService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updatePasswordDto: UpdatePasswordDto) {
  //   return this.passwordService.update(+id, updatePasswordDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.passwordService.remove(+id);
  // }
}
