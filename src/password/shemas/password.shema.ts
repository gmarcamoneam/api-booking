import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, ObjectId } from 'mongoose';


export type PasswordDocument = HydratedDocument<Password>;

@Schema()
export class Password {
  @Prop()
  email: string;

  @Prop()
  token: string;

}

export const PasswordSchema = SchemaFactory.createForClass(Password);