import { Controller, Get ,Res} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('confirm-now')
getEmailConfirmationWebPage(@Res() res){
return res.render('verification-success')
}
}
