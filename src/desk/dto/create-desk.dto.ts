import { ObjectId } from "mongoose";

export class CreateDeskDto {
    name:string;
    //id_room:string
    id_room : { type: ObjectId, ref: 'Room' }

}
