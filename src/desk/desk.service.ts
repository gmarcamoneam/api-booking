import { Injectable, NotFoundException } from '@nestjs/common';
import { Req } from '@nestjs/common/decorators';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from 'src/user/user.service';
// import { CreateOfficeDto } from 'src/office/dto/create-office.dto';
// import { Room } from 'src/room/entities/room.entity';
// import { RoomDocument } from 'src/room/shemas/room.shema';
import { CreateDeskDto } from './dto/create-desk.dto';
import { UpdateDeskDto } from './dto/update-desk.dto';
import { Desk, DeskDocument } from './shemas/desk.shema';

@Injectable()
export class DeskService {
  constructor(@InjectModel(Desk.name) private deskModel: Model<DeskDocument>) {}
  
  async createdesk(createDeskDto: CreateDeskDto): Promise<Desk> { 
    const newdesk = await new this.deskModel(createDeskDto)
    return newdesk.save(); 
  }

  async getAlldesks() : Promise<Desk[]> { 
    const listdesk = await this.deskModel.find().populate({path: "id_room", model:"Room"});
    if (!listdesk || listdesk.length == 0) { 
        throw new NotFoundException('Données des desks non trouvées !'); 
    } 
   return listdesk ; 
  }

  async getdeskbyid(deskId: string): Promise<Desk> { 
    const existingdesk = await this.deskModel.findById(deskId).exec();
    if (!existingdesk) { 
     throw new NotFoundException(`desk #${deskId} not found`); 
    } 
    return existingdesk  ; 
 }
 
  async updatedesk(deskId : string, updatedeskDto : UpdateDeskDto) : Promise<Desk> { 
    const existingdesk = await this.deskModel.findByIdAndUpdate(deskId, updatedeskDto, { new : true }) ;
   if (!existingdesk) { 
     throw new NotFoundException(`desk #${deskId} not found`); 
   } 
   return existingdesk ; 
}
  
  async deletedesk(deskid: string): Promise<Desk> { 
    const deletedesk = await this.deskModel.findByIdAndDelete(deskid);
   if (!deletedesk) { 
     throw new NotFoundException(`desk #${deskid} not found`); 
   } 
   return deletedesk ; 
}
// async userconnect(token:any){
//   this.userservice.getAdminConnected(token)
// }





}

