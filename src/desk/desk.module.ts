import { Module } from '@nestjs/common';
import { DeskService } from './desk.service';
import { DeskController } from './desk.controller';
import { Desk, DeskSchema } from './shemas/desk.shema';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { UserService } from 'src/user/user.service';
import { Test, TestingModule } from '@nestjs/testing';
import { use } from 'passport';
import { User } from 'src/user/shemas/user.shema';
import { forwardRef } from '@nestjs/common';
import { UserModule } from 'src/user/user.module';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [/*forwardRef(() => UserModule),*/MongooseModule.forFeature([{ name: Desk.name, schema: DeskSchema }])],
  controllers: [DeskController],
providers: [DeskService],
  exports: [DeskService],
})
export class DeskModule {}
