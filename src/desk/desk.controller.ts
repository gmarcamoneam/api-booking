import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Put ,Headers} from '@nestjs/common';
import { Req, UseGuards } from '@nestjs/common/decorators';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { UserService } from 'src/user/user.service';
import { DeskService } from './desk.service';
import { CreateDeskDto } from './dto/create-desk.dto';
import { UpdateDeskDto } from './dto/update-desk.dto';

@Controller('desk')
export class DeskController {
  constructor(private readonly deskService: DeskService) {}

  //  @UseGuards(AuthGuard('admin'))
  @Post('/create') 
  // @UseGuards(AuthGuard('admin'))
   async createdesk(@Res() response, @Body() createdeskDto: CreateDeskDto) { 
    // return response.json('dgfnfgbdfb')
  try { 
    const newdeske = await this.deskService.createdesk(createdeskDto);
    return response.status(HttpStatus.CREATED).json({ 
    message : 'desk a été créé avec succès',data: 
    newdeske,}); 
} catch (err) { 
    return response.status(HttpStatus.BAD_REQUEST).json({ 
    statusCode : 400, 
    message : 'Erreur : desk non créé !', 
    erreur : 'Bad Request' 
}); 
}   
}

 
  @Get('/getall') 
  async getdesks(@Res() response,/*@Headers('Authorization') auth: string,*/ @Req() req) { 
  try { 
    // req.headers.authorization.replace('Bearer ', '');

    const listdesk = await this.deskService.getAlldesks(); 
    return response.status(HttpStatus.OK).json({ 
    message : 'Toutes les données des desks ont été trouvées avec succès',data:listdesk,}); 
  } catch (err) { 
    return response.status(err.status).json(err.response); 
  } 
  }
 
  @Get('/:id') 
  async getofficebyid(@Res() response, @Param('id') deskId: string) { 
  try { 
   // const jwt =   req.headers.authorization.replace('Bearer ', '');

      const existingdesk= await this.deskService.getdeskbyid(deskId);
      return response.status(HttpStatus.OK).json({ 
      message : 'desk trouvé avec succès',data: existingdesk,}); 
  } catch (err) { 
     return response.status(err.status).json(err.response); 
  } 
  }
  
  @Put('/update/:id') 
async updateStudent(@Res() response,@Param('id') deskid: string, 
@Body() updatedeskDto: UpdateDeskDto) { 
  try { 
   const existingdesk = await this.deskService.updatedesk( deskid, updatedeskDto);
  return response.status(HttpStatus.OK).json({ 
  message : 'Le desk a été mis à jour avec succès', 
  data: existingdesk,});
} catch (err) { 
   return response.status(err.status).json(err.response);  
}  
} 
// @UseGuards(AuthGuard('admin'))
  @Delete('/delete/:id')  
  async deletedesk(@Res() response, @Param('id') deskid: string )
  {
    try { 
      const Delete = await this.deskService.deletedesk(deskid);
      return response.status(HttpStatus.OK).json({ 
      message : 'desk supprimé avec succès', 
      data:0}); 
    }catch (err) { 
      return response.status(err.status).json(err.response); 
    } 
  }
}
