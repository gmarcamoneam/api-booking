import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, ObjectId } from 'mongoose';


export type DeskDocument = HydratedDocument<Desk>;

@Schema({timestamps: true})
export class Desk {

  @Prop()
  name: string;

@Prop()
id_room : [{ type: ObjectId, ref: 'Room' }]

}

export const DeskSchema = SchemaFactory.createForClass(Desk);