import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  Req,
  Res,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './shemas/user.shema';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtService } from '@nestjs/jwt';
import passport from 'passport';
import {
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common/exceptions';
import { MailerService } from '@nestjs-modules/mailer';
import * as jwt from 'jsonwebtoken';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { randomBytes } from 'crypto';
import { response } from 'express';
import { join } from 'path';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private jwtService: JwtService,
    private maileservice: MailerService,
  ) {}
  async create(createUserDto: CreateUserDto) {
    try {
      const user = new User();
      const hashPassword = await bcrypt.hash(createUserDto.password, 10);
      user.fullname = createUserDto.fullname;
      user.email = createUserDto.email;
      user.password = hashPassword;
      user.status = createUserDto.status;
      user.phone = createUserDto.phone;
      // user.id = createUserDto.id;
      user.confirmed = createUserDto.confirmed;
      // user.verificationcode = createUserDto.verificationcode = randomBytes(6).toString("hex"),
      // this.sendMailRegisterUser(createUserDto);
      /*const user = */ //await this.utilservice.sendEmail(createUserDto.email, await  confirmemaillink(user.id))
      return this.userModel.create(user);
    } catch (err) {
      throw new Error(`Error creating ${err} user ${err.message}`);
    }
  }
  /*  hethi ela teb3a eka site */
  private sendMailRegisterUser(user): void {
    const domain = `http://localhost:3000/`;

    this.maileservice
      .sendMail({
        // to: user.email,
        // from: 'from@example.com',
        // subject: 'Registration successful ✔',
        // text: 'Registration successful!',
        // template: 'index',
        // context: {
        //   title: 'Registration successfully',
        //   description:
        //     "You did it! You registered!, You're successfully registered.✔",
        //   nameUser: user.name,
        // },

        // from: 'gmarcamoneam@gmail.com',
        to: user.email,
        subject: 'welcome' + ' ' + user.fullname,
        text: 'Bonjour Mr/Mme',
        html: `
                            <h2>Hello ${user.fullname}</h2>
                            <p>we are glad to have you on board at  ${user.fullname}. </p>
                            <a href="${domain}confirm-now/${user.verificationcode}">confirm now</a>
                     `,
      })
      .then((response) => {
        console.log(response);
        console.log('User Registration: Send Mail successfully!');
      })
      .catch((err) => {
        console.log(err);
        console.log('User Registration: Send Mail Failed!');
      });
  }
  // async verifyemail(createuserdto: CreateUserDto,@Res()res){
  //   try {
  //     const user = await this.userModel.findOne({verificationcode : createuserdto.verificationcode})
  //     user.confirmed = true;
  //     user.verificationcode = undefined;
  //     user.save();
  //   res.sendfile(join(__dirname, "../templates/verification_success.html"));
  //   } catch (error) {
  //   res.sendfile(join(__dirname, "../templates/errors.html"));

  //   }
  // }
  async getUser(email: string, password: string): Promise<User> {
    try {
      const user = await this.userModel.findOne({
        where: { email },
      });
      const isMatch = await bcrypt.compare(password, user.password);
      if (user && isMatch) {
        return user;
      } else {
        throw new Error(`User not found`);
      }
    } catch (err) {
      throw new Error(`Error finding ${err} user ${err.message}`);
    }
  }

  /*     getallsusers   */
  async getAllusers(): Promise<User[]> {
    const listuser = await this.userModel.find();
    if (!listuser || listuser.length == 0) {
      throw new NotFoundException('Données des users non trouvées !');
    }
    return listuser;
  }
  /* find user by email */
  async getByEmail(email: string) {
    const user = await this.userModel.findOne({ email });
    if (user) {
      return user;
    }
    throw new HttpException('email not found', HttpStatus.NOT_FOUND);
  }
  async getBypayload(payload: string) {
    const user = await this.userModel.findOne({ payload });
    if (user) {
      return user;
    }
    throw new HttpException('email not found', HttpStatus.NOT_FOUND);
  }

  async getById(id) {
    const user = await this.userModel.findById({ id });
    if (user) {
      return user;
    }
    throw new HttpException(
      'User with this id does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  // (...)
  async validateUser(payload: any): Promise<User> {
    return await this.userModel.findOne({ email: payload.email });
  }
  async forgetpassword(createUserDto: CreateUserDto): Promise<any> {
    const userUpdate = await this.getByEmail(createUserDto.email);
    const resetPasswordToken = randomBytes(6).toString('hex'); // randomStringGenerator.toString()
    console.log('resetPasswordToken ', resetPasswordToken);
    this.userModel.updateOne(
      {
        email: createUserDto.email,
      },
      {
        resetPasswordToken: resetPasswordToken,
        // reserPasswordToken: createUserDto.token
      },
      // {
      //   reserpasswordtokenAt: Date
      // }
    );

    // const passwordRand = Math.random()
    //   .toString(36)
    //   .slice(-8);
    // userUpdate.password = bcrypt.hashSync(passwordRand, 8);
    const token = jwt.sign(
      {
        id: userUpdate._id,
        user: userUpdate,
      },
      'secretKey',
      {
        expiresIn: '24h',
      },
    );

    this.sendMailForgotPassword(userUpdate.email, token);

    //return await this.userModel.create(userUpdate);
    return { message: 'Forgot Password: Send Mail successfully!!!!!!!!!' };
  }

  private sendMailForgotPassword(email, reserPasswordToken): void {
    const resetPasswordUrl = `http://localhost:4200/reset-password/${reserPasswordToken}`;

    this.maileservice
      .sendMail({
        to: email,
        subject: 'Reset your password',
        html: `Click <a href="${resetPasswordUrl}">here</a> to reset your password!`,
      })
      .then((response) => {
        // response.status ('chek your email!!, send mail successfully')
        console.log(response);
        console.log('Forgot Password: Send Mail successfully!');
      })
      .catch((err) => {
        //err.HttpStatus ('forgetassword : send mail failed')
        console.log(err);
        console.log('Forgot Password: Send Mail Failed!');
      });
  }

  async resetpassword(
    newpassword: string,
    reserPasswordToken: string,
  ): Promise<any> {
    // console.log(newpassword);
    console.log('newpassord', newpassword, typeof newpassword);

    try {
      const resetPasswordToken = reserPasswordToken;
      if (resetPasswordToken) { 
        console.log(resetPasswordToken);

        // pour verifier date d'expiration de resetPasswordtoken
        jwt.verify(resetPasswordToken, 'secretkey', async (x) => {
          if (!x) {
            return { message: 'incorrect token' };
            // throw new HttpException('incorrect token ', HttpStatus.NOT_FOUND);
          }
          const decoded = await this.validateToken(resetPasswordToken);
          console.log('decodetoken', decoded);

          const user = await this.userModel.findOne({
            reserPasswordToken: resetPasswordToken,
          });
          console.log(user);

          user.password = bcrypt.hashSync(newpassword, 10);
          user.save();
          return { message: 'password changée' };
        });
      }
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  async validateToken(reserPasswordToken: string): Promise<any> {
    try {
      // const secret = 'secretkey'
      const decoded = await jwt.decode(reserPasswordToken);
      return decoded;
    } catch (err) {
      return null;
    }
  }
  async updateUser(userId: string, updatedUser: Partial<User>): Promise<User> {
    const user = await this.userModel.findByIdAndUpdate(
      { _id: userId },
      updatedUser,
      { new: true },
    );
    return user;
  }
  async deleteUser(id: string): Promise<User> {
    const deletedUser = await this.userModel.findByIdAndDelete(id);
    if (!deletedUser) {
      // Si aucun utilisateur n'a été trouvé avec cet id, renvoie une erreur
      throw new NotFoundException(`Utilisateur d'ID ${id} non trouvé`);
    }
    return deletedUser;
  }
  async getuserbyid(id: string): Promise<User> {
    const existinguser = await this.userModel
      .findById(id)
      .populate({ path: 'id_Booking', model: 'Booking' })
      .exec();
    if (!existinguser) {
      throw new NotFoundException(`user #${id} not found`);
    }
    return existinguser;
  }
 
}
