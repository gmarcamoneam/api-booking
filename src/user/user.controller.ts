import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { UserService } from './user.service';

import { UpdateUserDto } from './dto/update-user.dto';
import * as bcrypt from 'bcrypt';
import { User } from './shemas/user.shema';
import { CreateUserDto } from './dto/create-user.dto';
import { Put, UseGuards } from '@nestjs/common/decorators';
import { AuthGuard } from '@nestjs/passport';

import { MailerService } from '@nestjs-modules/mailer/dist';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private mailerservice: MailerService,
  ) {}

  @Post('/signup')
  async create(@Body() createUserDto: CreateUserDto, @Res() res) {
    // return this.userService.create(createUserDto);
    try {
      const user = await this.userService.create(createUserDto);

      return res.status(HttpStatus.OK).json({
        message: 'User registration successfully!',
        status: 200,
        data: user,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: User not registration!',
        status: 400,
      });
    }
  }

  // @UseGuards(AuthGuard('admin'))
  @Get()
  async getusers(@Res() response) {
    try {
      const lisusres = await this.userService.getAllusers();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des users ont été trouvées avec succès',
        data: lisusres,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Post('/forget')
  forget(@Body() createuserdto: CreateUserDto) {
    return this.userService.forgetpassword(createuserdto);
  }
  //   @Post('/reset')
  //  async reset(@Body() body:any,@Res() res,){
  //     try {
  //           await this.userService.resetpassword(body.newpassword,body.token);

  //           return res.status(HttpStatus.OK).json({
  //             message: 'Request Change Password Successfully!',
  //             status: 200,
  //           });
  //         } catch (err) {
  //           return res.status(HttpStatus.BAD_REQUEST).json({
  //             message: 'Error: Change password failed!',
  //             status: 400,
  //           });
  //         }

  //   }

  @Post('/reset/:token')
  async reset(@Body() body: any, @Param('token') token: string, @Res() res) {
    try {
      await this.userService.resetpassword(body.newpassword, token);
      return res.status(HttpStatus.OK).json({
        message: 'Request Change Password Successfully!',
        status: 200,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: Change password failed!',
        status: 400,
      });
    }
  }

  @Put('/:userId/profile')
  async updateUser(
    @Param('id') id: string, @Body() updateUserDto: UpdateUserDto,
    @Res() res,
  ): Promise<User> {
    // const user = await this.userService.updateUser(userId, updatedUser);
    // return user;
    try {
      const user = await this.userService.updateUser(id, updateUserDto);

      return res.status(HttpStatus.OK).json({
        message: 'User Updated successfully!',
        status: 200,
        data: user,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: User not updated!',
        status: 400,
      });
    }
  }

  @Delete('/delete/:id')
  async deletusetr(@Res() response, @Param('id') userid: string) {
    try {
      const delet = await this.userService.deleteUser(userid);
      return response.status(HttpStatus.OK).json({
        message: 'user deleted successfully! ',
        /* data:delet*/
      });
    } catch (err) {
      // return response.status(err.status).json(err.response);
      return response.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: User not deleted!',
        status: 400,
      });
    }
  }
  @Get('/:id')
  async getuserbyid(@Res() response, @Param('id') Id: string) {
    try {
      const existinguser = await this.userService.getuserbyid(
        Id,
      );
      return response.status(HttpStatus.OK).json({
        message: 'user trouvé avec succès',
        data: existinguser,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.userService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
  //   return this.userService.update(+id, updateUserDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.userService.remove(+id);
  // }
}
