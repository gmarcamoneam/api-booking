import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';



export type UserDocument = HydratedDocument<User>;

@Schema({timestamps: true})
export class User {

  @Prop()
  fullname: string;
 
  @Prop({ required: true, unique: true })
  email: string;

  @Prop()
  password: string;

  @Prop()
  phone: string;

  @Prop()
  salt: string;
  
  @Prop()
  token: string;
  
  @Prop()
  refreshToken: string;

@Prop()
resetPasswordToken:string

@Prop()
resetPasswordTokenAt:string
  @Prop({
    type: Boolean,
    default:false
  })
  confirmed: Boolean
  
  @Prop({
    type: String,
    default:false
  })
  verificationcode: string

  @Prop({
    type: String,
    enum:['admin','employe'],
    default:'employe'
})
  status: 'admin' |'employe' = 'employe'

}

export const UserSchema = SchemaFactory.createForClass(User);


