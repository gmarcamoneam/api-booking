import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User } from './entities/user.entity';
import { UserSchema } from './shemas/user.shema';
 //import { LocalStrategy } from 'src/local.auth/local.auth.service';
// import { LocalStrategy } from "./local.strategy";
import { LocalStrategy } from "src/auth/local.strategy";

import { AuthService } from 'src/auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { PasswordService } from 'src/password/password.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';


@Module({
  imports: [ MailerModule.forRootAsync({
    useFactory: () => ({transport:
    {
      // host: "smtp.mailtrap.io",
      service:"gmail",
      host: "smtp.gmail.com",

      port: 2525,
      secure: true,
      auth: {
        // user:  "03aade3ded14ab",user de mailtrap
        user:  "gmarcamoneam@gmail.com",
        // pass:  "fb1c1dbaecd53f" mot de passe de mailtrap
        pass:  "rwrcwtddvclchzpf"

      },
    },
    defaults: {
      from: '"nest-modules" <modules@nestjs.com>',
    },
    // template: {
    //   dir: process.cwd() + './templates/emails',
    //   adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
    //   options: {
    //     strict: true,
    //   },
    // },
  })
  }),MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],
  controllers: [UserController],
  providers: [UserService,LocalStrategy,AuthService,JwtService]
})
export class UserModule {}
