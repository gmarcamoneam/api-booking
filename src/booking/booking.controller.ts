import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  BadRequestException,
} from '@nestjs/common';
import { Put, Req, UseGuards } from '@nestjs/common/decorators';
import { AuthGuard } from '@nestjs/passport';
import { response } from 'express';

import { BookingService } from './booking.service';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';

@Controller('booking')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

  // @Post()
  // async createBooking(@Res() response, @Body() createbookingDto: CreateBookingDto ) {
  //   try {
  //     const newbooking = await this.bookingService.createbooking(
  //       createbookingDto,
  //     );
  //     return response.status(HttpStatus.CREATED).json({
  //       message: 'booking a été créé avec succès',
  //       data: newbooking,
  //     });
  //   } catch (err) {
  //     return response.status(HttpStatus.BAD_REQUEST).json({
  //       statusCode: 400,
  //       message: 'Erreur : booking non créé !',
  //       erreur: 'Bad Request',
  //     });
  //   }
  // }
  // @UseGuards(AuthGuard('local'))
  @Get('/getall')
  // @UseGuards(AuthGuard('admin'))
  async getbooking(@Res() response) {
    try {
      const listbooking = await this.bookingService.getAllbookings();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des booking ont été trouvées avec succès',
        data: listbooking,
      });
    } catch (err) {
      // return response.status(err.status).json(err.response);
      // return response.status(500).json(err.response);

      return response.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Une erreur s'est produite lors de la récupération des données de réservation",
        error: err.message,
      });
    }
  }
  @Get('/:id')
  async getbookingbyid(@Res() response, @Param('id') bookingId: string) {
    try {
      const existingbooking = await this.bookingService.getbookingbyid(
        bookingId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'booking trouvé avec succès',
        data: existingbooking,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Put('/update/:id')
  async updatebooking(
    @Res() response,
    @Param('id') bookingid: string,
    @Body() updatebookingDto: UpdateBookingDto,
  ) { 
    try {
      const existingbooking = await this.bookingService.updatebooking(
        bookingid,
        updatebookingDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Le booking a été mis à jour avec succès',
        data: existingbooking,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Delete('/delete/:id')
  async deletebooking(@Res() response, @Param('id') bookingid: string) {
    try {
      const Delete = await this.bookingService.deletebooking(bookingid);
      return response.status(HttpStatus.OK).json({
        message: 'booking supprimé avec succès',
        data: 0,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Post('/createbooking')
  async create(@Res() response, @Body() createbookingDto: CreateBookingDto) {
    // return this.bookingService.createBooking(createbookingDto);
    try {
      const newbooking = await this.bookingService.createbooking(
        createbookingDto,
      );
      return response.status(HttpStatus.CREATED).json({
        message: 'booking a été créé avec succès',
        data: newbooking,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Erreur : booking non créé !',
        erreur: 'Bad Request',
      });
    }
  }
}
