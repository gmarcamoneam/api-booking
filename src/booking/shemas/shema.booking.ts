import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { /*Date,*/ HydratedDocument, ObjectId } from 'mongoose';
import { Desk, DeskSchema } from 'src/desk/shemas/desk.shema';
import { RoomSchema } from 'src/room/shemas/room.shema';
import { UserSchema } from 'src/user/shemas/user.shema';

export type BookingDocument = HydratedDocument<Booking>;

@Schema({timestamps: true})
export class Booking {
  @Prop()
  start_date: string;

  @Prop()
  end_date: string;

  @Prop(UserSchema)
  id_user: string;

  @Prop(RoomSchema)
  id_room: string;
  

  @Prop(DeskSchema)
  id_desk: string; 
  
  
}

export const BookingSchema = SchemaFactory.createForClass(Booking);
