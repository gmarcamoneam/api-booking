import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
// import { Booking } from './entities/booking.entity';
import { Booking, BookingDocument } from './shemas/shema.booking';

@Injectable()
export class BookingService {
  constructor(
    @InjectModel(Booking.name) private bookingModel: Model<BookingDocument>,
  ) {}
  // async createbooking(createbookingDto: CreateBookingDto): Promise<Booking> {
  //   const newbooking = await new this.bookingModel(createbookingDto);
  //   return newbooking.save();
  // }
  async createbooking(createbookingDto: CreateBookingDto): Promise<Booking> {
    // Vérifier la disponibilité de la desk ou de la room
    const overlappingBooking = await this.bookingModel.findOne({
      $and: [
        {
          start_date: {
            $gte: createbookingDto.start_date,
            $lt: createbookingDto.end_date,
          },
        },
        {
          end_date: {
            $gt: createbookingDto.start_date,
            $lte: createbookingDto.end_date,
          },
        },
      ],
    });

    if (overlappingBooking) {
      throw new Error(
        'La chaise ou la maison est déjà réservée pour cette période',
      );
    }

    // Créer une nouvelle réservation
    const booking = await new this.bookingModel(createbookingDto);

    return await booking.save();
  }
  async getAllbookings(): Promise<Booking[]> {
    const listbooking = await this.bookingModel
      .find()
      .populate({ path: 'id_room', model: 'Room' })
    .populate({ path: 'id_desk', model: 'Desk' })
    .populate({ path: 'id_user', model: 'User' });
    if (!listbooking || listbooking.length == 0) {
      throw new NotFoundException('Données des booking non trouvées !');
    }
    return listbooking;
  }
  async getbookingbyid(bookinkgid: string): Promise<Booking> {
    const existingbooking = await this.bookingModel
      .findById(bookinkgid)
      .populate({ path: 'id_room', model: 'Room' })
      .populate({ path: 'id_desk', model: 'Desk' })
      .populate({ path: 'id_user', model: 'User' })
      .exec();
    if (!existingbooking) {
      throw new NotFoundException(`booking #${bookinkgid} not found`);
    }
    return existingbooking;
  }
  async updatebooking(
    bookingId: string,
    updatebookingDto: UpdateBookingDto,
  ): Promise<Booking> {
    const existingbooking = await this.bookingModel.findByIdAndUpdate(
      bookingId,
      updatebookingDto,
      { new: true },
    );
    if (!existingbooking) {
      throw new NotFoundException(`booking #${bookingId} not found`);
    }
    return existingbooking;
  }
  async deletebooking(bookingid: string): Promise<Booking> {
    const deletebooking = await this.bookingModel.findByIdAndDelete(bookingid);
    if (!deletebooking) {
      throw new NotFoundException(`desk #${bookingid} not found`);
    }
    return deletebooking;
  }
}
