import { ObjectId } from "mongoose";

export class Booking {
    start_date: Date;
    end_date:Date;
    // id_room : { type: ObjectId, ref: 'Room' };
    // id_desk : { type: ObjectId, ref: 'Desk' };
    // id_user : { type: ObjectId, ref: 'User' }
    id_room:string;
    id_user:string;
    id_desk:string
}
