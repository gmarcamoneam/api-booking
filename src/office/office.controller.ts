import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Put } from '@nestjs/common';
import { OfficeService } from './office.service';
import { CreateOfficeDto } from './dto/create-office.dto';
import { UpdateOfficeDto } from './dto/update-office.dto';
import { find, Observable, of } from 'rxjs';
import { UploadedFile, UseInterceptors } from '@nestjs/common/decorators';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';

// export const storage = {
//   storage: diskStorage({
//     destination: './upload' ,
//     filename: (req, file, cb) =>{
//       const filename: string = path.parse(file.originalname).name.replace(/\$/g, '') + uuidv4();
//       const extension: string = path.parse(file.originalname).ext;

//       cb(null, `${filename}${extension}`)
//     }
//   }) 
// }
@Controller('office')
export class OfficeController {
  constructor(private readonly officeService: OfficeService) {}
  @Post('/create') 
  //@UseInterceptors(FileInterceptor('file', storage))
   async createoffice(@Res() response, @Body() createofficeDto: CreateOfficeDto/*, @UploadedFile() file*/) { 
  try { 
    const newoffice = await this.officeService.createoffice(createofficeDto);
    return response.status(HttpStatus.CREATED).json({ 
    message : 'office a été créé avec succès',data: 
    newoffice,}); 
} catch (err) { 
    return response.status(HttpStatus.BAD_REQUEST).json({ 
    statusCode : 400, 
    message : 'Erreur : office non créé !', 
    erreur : 'Bad Request' 
}); 
} 
}
  @Get('/getalloffice')   
async getoffices(@Res() response) { 
try { 
  const listoffice = await this.officeService.getAlloffices(); 
  return response.status(HttpStatus.OK).json({ 
  message : 'Toutes les données des offices ont été trouvées avec succès',data:listoffice,}); 
} catch (err) { 
  return response.status(err.status).json(err.response); 
} 
}
  @Get('/:id') 
async getofficebyid(@Res() response, @Param('id') officeId: string) { 
try { 
    const existingoffice = 
await this.officeService.getofficebyid(officeId);
    return response.status(HttpStatus.OK).json({ 
    message : 'office trouvé avec succès',data: existingoffice,}); 
} catch (err) { 
   return response.status(err.status).json(err.response); 
} 
}
  @Put('/update/:id') 
async updateStudent(@Res() response,@Param('id') officeid: string, 
@Body() updateofficeDto: UpdateOfficeDto) { 
  try { 
   const existingoffice = await this.officeService.updateoffice( officeid, updateofficeDto);
  return response.status(HttpStatus.OK).json({ 
  message : 'Loffice a été mis à jour avec succès', 
  data: existingoffice,});
} catch (err) { 
   return response.status(err.status).json(err.response); 
} 
}
  @Delete('/delete/:id') 
async deleteoffice(@Res() response, @Param('id') officeid: string )
{
  try { 
    const delet = await this.officeService.deleteoffice(officeid);
    return response.status(HttpStatus.OK).json({ 
    message : 'office supprimé avec succès', 
    data:0}); 
  }catch (err) { 
    return response.status(err.status).json(err.response); 
  } 
} 
// @Post('upload')
// @UseInterceptors(FileInterceptor('file', storage))
// uploadFile(@UploadedFile() file): Observable<Object> {
//   console.log(file);
//   return of({imagePath: file.filename})
// }
}

