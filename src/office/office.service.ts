import { Injectable } from '@nestjs/common';
import { CreateOfficeDto } from './dto/create-office.dto';
import { UpdateOfficeDto } from './dto/update-office.dto';
import { Office, OfficeDocument } from './shemas/office.shema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NotFoundException } from '@nestjs/common/exceptions';

@Injectable()
export class OfficeService {
  constructor(
    @InjectModel(Office.name) private officeModel: Model<OfficeDocument>,
  ) {}
  async createoffice(CreateOfficeDto: CreateOfficeDto): Promise<Office> {
    const newoffice = await new this.officeModel(CreateOfficeDto);
    return newoffice.save();
  }
  async getAlloffices(): Promise<Office[]> {
    const listoffice = await this.officeModel.find();
    if (!listoffice || listoffice.length == 0) {
      throw new NotFoundException('Données des étudiants non trouvées !');
    }
    return listoffice;
  }
  async getofficebyid(officeId: string): Promise<Office> {
    const existingoffice = await this.officeModel.findById(officeId).exec();
    if (!existingoffice) {
      throw new NotFoundException(`Office #${officeId} not found`);
    }
    return existingoffice;
  }
  async updateoffice(
    officeId: string,
    updateOfficeDto: UpdateOfficeDto,
  ): Promise<Office> {
    const existingoffice = await this.officeModel.findByIdAndUpdate(
      officeId,
      updateOfficeDto,
      { new: true },
    );
    if (!existingoffice) {
      throw new NotFoundException(`Student #${officeId} not found`);
    }
    return existingoffice;
  }
  async deleteoffice(officeid: string): Promise<Office> {
    const deleteoffice = await this.officeModel.findByIdAndDelete(officeid);
    if (!deleteoffice) {
      throw new NotFoundException(`Office #${officeid} not found`);
    }
    return deleteoffice;
  }
}
