import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type OfficeDocument = HydratedDocument<Office>;

@Schema({timestamps: true})    
export class Office {

  @Prop()
  name: string;
   

  @Prop()
  address: string;    

  @Prop()
  phone: string;



}

export const OfficeSchema = SchemaFactory.createForClass(Office);