import { UnauthorizedException } from "@nestjs/common";
import { Injectable } from "@nestjs/common/decorators";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { CreateUserDto } from "src/user/dto/create-user.dto";
import { AuthService } from "./auth.service";


@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy){
    constructor(private authservice: AuthService){
        super()
    }
 
    async validate(/*email: string, password: string*/userto:CreateUserDto): Promise<any>{
        const user = await this.authservice.validateUser(userto);
        if(!user){
            throw new UnauthorizedException()
        }
        return user;
    }
}