// import { Module } from '@nestjs/common';
// import { AuthService } from './auth.service';
// import { AuthController } from './auth.controller';
// import { UserModule } from 'src/user/user.module';
// import { JwtModule } from '@nestjs/jwt';
// import { MongooseModule } from '@nestjs/mongoose';
// import { UserSchema } from 'src/user/shemas/user.shema';
// import { UserService } from 'src/user/user.service';
// import { PassportModule } from "@nestjs/passport";
// // import { LocalStrategy } from 'src/local.auth/local.auth.service';
// // import { LocalStrategy } from './local-strategy';

// @Module({
//   imports: [UserModule, PassportModule, JwtModule.register({
//     secret: 'secretKey',
//     signOptions: { expiresIn: '60s' },
//   }), MongooseModule.forFeature([{ name: "User", schema: UserSchema }])],
//   controllers: [AuthController],
// providers: [AuthService, UserService,/* LocalStrategy*/]
// })
// export class AuthModule {}











import { Module } from "@nestjs/common"
import { UserModule } from "src/user/user.module";
import { AuthService } from "./auth.service"
import { PassportModule } from "@nestjs/passport"
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { UserService } from "src/user/user.service";
import { MongooseModule } from "@nestjs/mongoose"
import { User, UserSchema } from "src/user/shemas/user.shema";
import { LocalStrategy } from "./local.strategy";
import { AdminStrategy } from "./admin.strategy";

// import { MailService } from "src/mail/mail.service";
//import { RoleStrategy } from "./roles.strategy";



@Module({
  imports: [AuthModule, PassportModule.register({ defaultStrategy: 'local' }), JwtModule.register({
     secret: 'secretKey', 
    signOptions: { expiresIn: '10h' },
  }),MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],
providers: [AuthService, UserService, LocalStrategy,AdminStrategy,/*RoleStrategy*//*MailService*/],
  controllers: [AuthController],
})
export class AuthModule { }