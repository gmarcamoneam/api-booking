// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { PassportStrategy } from '@nestjs/passport';
// import { Strategy } from 'passport-jwt';
// import { AuthService } from './auth.service';

// @Injectable()
// export class RoleStrategy extends PassportStrategy(Strategy) {
//   constructor(private authService: AuthService) {
//     super({
//       jwtFromRequest: req => req.headers.authorization,
//       secretOrKey: 'secretKey',
//     });
//   }

//   async validate(payload) {
//     const user = await this.authService.validateUser(payload);
//     if (!user) {
//       throw new UnauthorizedException();
//     }
//     return user;
//   }
// }