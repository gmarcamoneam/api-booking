import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/entities/user.entity';
import {
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common/exceptions';

import { CreateUserDto } from 'src/user/dto/create-user.dto';
@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private jwtService: JwtService /*, private userModel:UserModule*/,
  ) {}
  //async validateUser(email: string, password: string): Promise<any> {
  async validateUser(authDto: CreateUserDto): Promise<User> {
    const { email, password } = authDto;
    const user = await this.userService.getByEmail(email);
    console.log('user', user);
    if (!user) {
      throw new NotFoundException('email not found !');
    }
    const passwordCompare = bcrypt.compareSync(password, user.password);
    console.log('passwordCompare', passwordCompare);
    if (!passwordCompare) {
      throw new NotFoundException('password incorrect!');
    } else {
      return user;
    }
  }
  /* login existing user */
  async login(CreateUserDto: CreateUserDto) {
    const user = await this.validateUser(CreateUserDto);
    console.log('user', user);

    const payload = {
      // status: user.status,
      //id: user.id,
      //  email : user.email,
      user: user,
    }; 

    return {
      //  user:CreateUserDto,
       message:"user a été crée avec succée",
      user,
      token: this.jwtService.sign(payload),
    };
  }
} 
   