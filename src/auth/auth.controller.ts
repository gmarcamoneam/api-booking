// // import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
// // import { AuthService } from './auth.service';
// // import { CreateAuthDto } from './dto/create-auth.dto';
// // import { UpdateAuthDto } from './dto/update-auth.dto';
// import { Controller, Request, Post, UseGuards } from '@nestjs/common';
// import { AuthService } from './auth.service';
// import { AuthGuard } from '@nestjs/passport';


// @Controller('auth')
// export class AuthController {
//   constructor(private authService: AuthService) { }

//   @UseGuards(AuthGuard('local'))
//   @Post('/login')
//   async login(@Request() req) {
//       return this.authService.login(req.user);
//   }
// }
// export class AuthController {
//   constructor(private readonly authService: AuthService) {}

//   @Post()
//   create(@Body() createAuthDto: CreateAuthDto) {
//     return this.authService.create(createAuthDto);
//   }

//   @Get()
//   findAll() {
//     return this.authService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.authService.findOne(+id);
//   }

//   @Patch(':id')
//   update(@Param('id') id: string, @Body() updateAuthDto: UpdateAuthDto) {
//     return this.authService.update(+id, updateAuthDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.authService.remove(+id);
//   }
// }
















import { Controller, Request, Post, UseGuards, Body, HttpStatus, Get, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import * as bcrypt from 'bcrypt';
import { CreateAuthDto } from './dto/create-auth.dto';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { session } from 'passport';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { User } from 'src/user/shemas/user.shema';
import { UserService } from 'src/user/user.service';
//import { LocalAuthGuard } from 'src/local.auth/guard.auth.service';


@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService,private userservice: UserService) { }


    @Post('/login')
    async login(@Body() CreateUserDto: CreateUserDto/*,@Res() res,*/) {
        return this.authService.login(CreateUserDto);
  
    }
}