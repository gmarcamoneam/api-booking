// import { Injectable } from "@nestjs/common";
// import { JwtService } from "@nestjs/jwt";
// import { AuthService } from "./auth.service";
// import {CanActivate, ExecutionContext} from "@nestjs/common"
// @Injectable()
// export class RoleGuard implements CanActivate {
//   constructor(private authService: AuthService, private jwtService: JwtService) {}

//   async canActivate(context: ExecutionContext): Promise<boolean> {
//     const request = context.switchToHttp().getRequest();
//     const token = request.headers.authorization.split(' ')[1];
//     const payload = await this.jwtService.decode(token);
//     const user = await this.authService.getById(payload);
//     request.user = user;
//     if (user.status === 'admin') {
//       return true;
//     }}}