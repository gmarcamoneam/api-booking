import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt,Strategy } from "passport-jwt";
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    constructor(){
        super({
            jwtFromRequestr: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExeption: false,
            secretOrKey: 'secretKey'
        })
    }
    async validate(payload: any){
       // const user = await this.userservice.getById(payload.sub)
        return {
            id: payload.sub,
            name: payload.name,
           // ...user
        }
    }

}