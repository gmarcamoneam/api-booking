import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, ObjectId } from 'mongoose';
export type RoomDocument = HydratedDocument<Room>;
@Schema({timestamps: true})
export class Room {
  @Prop()
  name: string;
  @Prop()
  type: string;
  @Prop()
  id_office: [{ type: ObjectId; ref: 'Office' }];
}
export const RoomSchema = SchemaFactory.createForClass(Room);
