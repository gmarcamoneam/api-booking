import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { Room, RoomDocument } from './shemas/room.shema';

@Injectable()
export class RoomService {
  constructor(
    @InjectModel(Room.name) private roomModel: Model<RoomDocument>,
  ) {}
  async createroom(CreateRoomDto: CreateRoomDto): Promise<Room> {
    const newroom = await new this.roomModel(CreateRoomDto);
    return newroom.save();
  }
  async getAllrooms(): Promise<Room[]> {
    const listroom = await this.roomModel.find(); //.populate({path:"id_office",model:"Office"});
    if (!listroom || listroom.length == 0) {
      throw new NotFoundException('Données des room non trouvées !');
    }
    return listroom;
  }
  async getroombyid(roomId: string): Promise<Room> {
    const existingroom = await this.roomModel.findById(roomId).exec();
    if (!existingroom) {
      throw new NotFoundException(`Room #${roomId} not found`);
    }
    return existingroom;
  }
  async updateroom(
    roomId: string,
    updateroomDto: UpdateRoomDto,
  ): Promise<Room> {
    const existingroom = await this.roomModel.findByIdAndUpdate(
      roomId,
      updateroomDto,
      { new: true },
    );
    if (!existingroom) {
      throw new NotFoundException(`Room #${roomId} not found`);
    }
    return existingroom;
  }
  async deleteroom(roomid: string): Promise<Room> {
    const deleteroom = await this.roomModel.findByIdAndDelete(roomid);
    if (!deleteroom) {
      throw new NotFoundException(`Room #${roomid} not found`);
    }
    return deleteroom;
  }
}
