import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  Put,
} from '@nestjs/common';
import { RoomService } from './room.service';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';

@Controller('room')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Post('/create')
  async createroom(
    @Res() response,
    @Body() createroomDto: CreateRoomDto /*, @UploadedFile() file*/,
  ) {
    try {
      const newoffice = await this.roomService.createroom(createroomDto);
      return response.status(HttpStatus.CREATED).json({
        message: 'room a été créé avec succès',
        data: newoffice,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Erreur : room non créé !',
        erreur: 'Bad Request',
      });
    }
  }
  @Get('/getallroom')
  async getorooms(@Res() response) {
    try {
      const listroom = await this.roomService.getAllrooms();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des rooms ont été trouvées avec succès',
        data: listroom,
      });
    } catch (err) {
      response.status(400).send({ message: err.message });
    }
  }
  @Get('/getbyid/:id')
  async getofficebyid(@Res() response, @Param('id') roomId: string) {
    try {
      const existingroom = await this.roomService.getroombyid(roomId);
      return response.status(HttpStatus.OK).json({
        message: 'room trouvé avec succès',
        data: existingroom,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Put('/update/:id')
  async updateStudent(
    @Res() response,
    @Param('id') roomid: string,
    @Body() updateroomDto: UpdateRoomDto,
  ) {
    try {
      const existingroom = await this.roomService.updateroom(
        roomid,
        updateroomDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Le room a été mis à jour avec succès',
        data: existingroom,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Delete('/delete/:id')
  async deleteroom(@Res() response, @Param('id') roomid: string) {
    try {
      const Delete = await this.roomService.deleteroom(roomid);
      return response.status(HttpStatus.OK).json({
        message: 'room supprimé avec succès',
        data: 0,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
